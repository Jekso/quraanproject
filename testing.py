import numpy as np
import librosa
import pandas as pd
import os
from tensorflow.keras.models import load_model
from pydub import AudioSegment
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split 
from sklearn import metrics 
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten
from tensorflow.keras.layers import Convolution2D, Conv2D, MaxPooling2D, GlobalAveragePooling2D
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import ModelCheckpoint 

def extract_features(file_name):
    try:
        audio, sample_rate = librosa.load(file_name, res_type='kaiser_fast') 
        mfccs = librosa.feature.mfcc(y=audio, sr=sample_rate, n_mfcc=40)
        pad_width = 174 - mfccs.shape[1]
        mfccs = np.pad(mfccs, pad_width=((0, 0), (0, pad_width)), mode='constant')
        
    except Exception as e:
        print(e)
        return None 
     
    return mfccs



def print_prediction(file_name):
    model = load_model('weights.best.basic_cnn.hdf5')
    le = LabelEncoder()
    le.classes_ = np.load('classes.npy')

    prediction_feature = extract_features(file_name) 
    prediction_feature = prediction_feature.reshape(1, 40, 174, 1)
    predicted_vector = model.predict_classes(prediction_feature)
    predicted_class = le.inverse_transform(predicted_vector) 
    print("The predicted class is:", predicted_class[0], '\n') 

    predicted_proba_vector = model.predict_proba(prediction_feature) 
    predicted_proba = predicted_proba_vector[0]
    for i in range(len(predicted_proba)): 
        category = le.inverse_transform(np.array([i]))
        print(category[0], "\t\t : ", format(predicted_proba[i], '.32f') )



file_name = '051.mp3' # put ur file path here 
file = file_name.split('.')[0]
ext = file_name.split('.')[1]
if  ext == 'mp3':
    sound = AudioSegment.from_mp3(file_name)
    file_name = f'{file}.wav'
    sound.export(file_name, format="wav")
print_prediction(file_name)